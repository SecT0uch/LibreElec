#!python

import requests
import configparser
import os
import sys


os.chdir(os.path.dirname(sys.argv[0]))    # We change working dir to script location

config = configparser.RawConfigParser()
config.optionxform = str                  # For case sensitive config file
config.read('./.config')

lastip = config.get("main", "lastip")

# Gesting public IP address

public_ip = requests.get('https://api.ipify.org').text


if public_ip != lastip:
    porkUser = config.get("main", "user")
    porkPass = config.get("main", "pass")
    fqdn = config.get("main", "fqdn")

    hostname = fqdn.split('.')[0]
    domain = fqdn.split('.', 1)[1]

    # Login to porkbun

    session = requests.Session()                # On définit une session (pour garder les cookies)

    set_cookies = session.get('https://porkbun.com/account/login').headers.get('Set-Cookie')
    csrf_pb = set_cookies.split('csrf_pb=')[1].split(';')[0]

    data = {'username': porkUser, 'password': porkPass, 'uri': '/account/login', 'isajax': 'true', 'csrf_pb': csrf_pb}
    r = session.post('https://porkbun.com/api/user/login', cookies={'csrf_pb': csrf_pb}, data=data)
    csrf_pb = r.headers.get('Set-Cookie').split('csrf_pb=')[1].split(';')[0]

    # Get recordID

    data = {'domain': domain, 'isajax': 'true', 'csrf_pb': csrf_pb}
    r = session.post('https://porkbun.com/api/domains/getDomainDNS', cookies={'csrf_pb': csrf_pb}, data=data)
    csrf_pb = r.headers.get('Set-Cookie').split('csrf_pb=')[1].split(';')[0]
    dnsRecords = r.json().get('dnsRecords')
    for record in dnsRecords:
        if record.get('name') == 'home.sect0uch.world':
            recordID = record.get('id')
            break

    # Save A record

    data = {'dnsRecord[domain]': domain, 'dnsRecord[host]': hostname, 'dnsRecord[ttl]': '300', 'dnsRecord[answer]': public_ip, 'dnsRecord[type]': 'A', 'dnsRecord[id]': recordID, 'isajax': 'true', 'csrf_pb': csrf_pb}
    r = session.post('https://porkbun.com/api/domains/editDomainDNS', cookies={'csrf_pb': csrf_pb}, data=data)

    # Replace lastip value in conf
    config.set('main', 'lastip', public_ip)
    with open('.config', 'w') as configfile:
        config.write(configfile)


#!python

import requests
import ConfigParser
import os
import sys


os.chdir(os.path.dirname(sys.argv[0]))    # We change working dir to script location

config = ConfigParser.RawConfigParser()
config.optionxform = str                  # For case sensitive config file
config.read('./.config')

lastip = config.get("main", "lastip")

# Gesting public IP address

public_ip = requests.get('https://api.ipify.org').text


if public_ip != lastip:

    vultrAPIKey = config.get("main", "vultrAPIKey")
    vultrFWGroupID = config.get("main", "vultrFWGroupID")
    comment = config.get("main", "comment")

    r = requests.get('https://api.vultr.com/v1/firewall/rule_list?FIREWALLGROUPID=' + vultrFWGroupID + '&direction=in&ip_type=v4', headers={'API-Key': vultrAPIKey})
    for rule in r.json().iteritems():
        if rule[1].get('subnet') == lastip:
            rulenum = rule[0]
            port = rule[1].get('port')

            # We delete the rule :
            requests.post('https://api.vultr.com/v1/firewall/rule_delete', headers={'API-Key': vultrAPIKey}, data={'FIREWALLGROUPID': vultrFWGroupID, 'rulenumber': rulenum})
            # And create a new one :
            data = {'FIREWALLGROUPID': vultrFWGroupID, 'direction': 'in', 'ip_type': 'v4', 'protocol': 'tcp', 'subnet': public_ip, 'subnet_size': '32', 'port': port, 'notes': comment}
            requests.post('https://api.vultr.com/v1/firewall/rule_create', headers={'API-Key': vultrAPIKey}, data=data)

    # Replace lastip value in conf
    config.set('main', 'lastip', public_ip)
    with open('.config', 'w') as configfile:
        config.write(configfile)
